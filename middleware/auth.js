import cookieParser from "cookie-parser"
import TodoList from "../models/todoList.js"

//check auth
async function auth(req, res, next){
    const {id} = req.cookies
    
    try{
        const user = await TodoList.findById(id)
        if (!user){
            return res.status(400).json({message : 'Login first'})
        }

        next()

    } catch(err) {
        console.log(err.message);
        return res.status(500).send({message : 'Error!'})
    }
}

export default auth