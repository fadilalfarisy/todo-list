import express from "express"
import bodyParser from "body-parser"
import cookieParser from "cookie-parser"
import auth from "../middleware/auth.js"
import item from "../controllers/itemController.js"
import todo from "../controllers/todoController.js"

const router = express.Router()

//set up middleware
router.use(bodyParser.json())
router.use(bodyParser.urlencoded({ extended: false }))
router.use(cookieParser())

//show item todo by id
router.get('/details', auth, item.getItemById)

//add item todo
router.post('/add', auth, item.addItem, todo.getTodos)

//edit item todo
router.put('/edit', auth, item.editItem, todo.getTodos)

//delete item todo
router.post('/delete', auth, item.deleteItem, todo.getTodos)

//edit status todo item
router.put('/editStatus', auth, item.editStatusItem, todo.getTodos)

export default router