import express from "express"
import bodyParser from "body-parser"
import cookieParser from "cookie-parser"
import user from "../controllers/userController.js"

const router = express.Router()

//set up middleware
router.use(bodyParser.json())
router.use(cookieParser())

//register
router.post('/register', user.register)

//login
router.post('/login', user.login)

//logout
router.get('/logout', user.logout)

export default router